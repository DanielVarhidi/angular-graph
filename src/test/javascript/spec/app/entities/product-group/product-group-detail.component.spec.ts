/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GraphTestModule } from '../../../test.module';
import { ProductGroupDetailComponent } from 'app/entities/product-group/product-group-detail.component';
import { ProductGroup } from 'app/shared/model/product-group.model';

describe('Component Tests', () => {
    describe('ProductGroup Management Detail Component', () => {
        let comp: ProductGroupDetailComponent;
        let fixture: ComponentFixture<ProductGroupDetailComponent>;
        const route = ({ data: of({ productGroup: new ProductGroup(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GraphTestModule],
                declarations: [ProductGroupDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ProductGroupDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProductGroupDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.productGroup).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
