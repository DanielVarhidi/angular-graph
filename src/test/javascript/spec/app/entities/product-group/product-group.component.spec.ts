/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GraphTestModule } from '../../../test.module';
import { ProductGroupComponent } from 'app/entities/product-group/product-group.component';
import { ProductGroupService } from 'app/entities/product-group/product-group.service';
import { ProductGroup } from 'app/shared/model/product-group.model';

describe('Component Tests', () => {
    describe('ProductGroup Management Component', () => {
        let comp: ProductGroupComponent;
        let fixture: ComponentFixture<ProductGroupComponent>;
        let service: ProductGroupService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GraphTestModule],
                declarations: [ProductGroupComponent],
                providers: []
            })
                .overrideTemplate(ProductGroupComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ProductGroupComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductGroupService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new ProductGroup(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.productGroups[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
