package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.ProductGroupDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing ProductGroup.
 */
public interface ProductGroupService {

    /**
     * Save a productGroup.
     *
     * @param productGroupDTO the entity to save
     * @return the persisted entity
     */
    ProductGroupDTO save(ProductGroupDTO productGroupDTO);

    /**
     * Get all the productGroups.
     *
     * @return the list of entities
     */
    List<ProductGroupDTO> findAll();


    /**
     * Get the "id" productGroup.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ProductGroupDTO> findOne(Long id);

    /**
     * Delete the "id" productGroup.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
