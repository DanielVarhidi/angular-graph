package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the ProductGroup entity.
 */
public class ProductGroupDTO implements Serializable {

    private Long id;

    private String productGroupName;

    private String type;

    private Long productGroupId;

    private Long parentProductGroupId;

    private Set<ProductDTO> products = new HashSet<>();

    private Set<ProductGroupDTO> childProductGroups = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductGroupName() {
        return productGroupName;
    }

    public void setProductGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getProductGroupId() {
        return productGroupId;
    }

    public void setProductGroupId(Long productGroupId) {
        this.productGroupId = productGroupId;
    }

    public Long getParentProductGroupId() {
        return parentProductGroupId;
    }

    public void setParentProductGroupId(Long productGroupId) {
        this.parentProductGroupId = productGroupId;
    }

    public Set<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDTO> products) {
        this.products = products;
    }

    public Set<ProductGroupDTO> getChildProductGroups() {
        return childProductGroups;
    }

    public void setChildProductGroups(Set<ProductGroupDTO> childProductGroups) {
        this.childProductGroups = childProductGroups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductGroupDTO productGroupDTO = (ProductGroupDTO) o;
        if (productGroupDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), productGroupDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProductGroupDTO{" +
            "id=" + getId() +
            ", productGroupName='" + getProductGroupName() + "'" +
            ", type='" + getType() + "'" +
            ", productGroup=" + getProductGroupId() +
            ", parentProductGroup=" + getParentProductGroupId() +
            "}";
    }
}
