package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.ProductGroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ProductGroup and its DTO ProductGroupDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProductGroupMapper extends EntityMapper<ProductGroupDTO, ProductGroup> {

    @Mapping(source = "productGroup.id", target = "productGroupId")
    @Mapping(source = "parentProductGroup.id", target = "parentProductGroupId")
    ProductGroupDTO toDto(ProductGroup productGroup);

    //@Mapping(target = "products", ignore = true)
    @Mapping(source = "productGroupId", target = "productGroup")
    //@Mapping(target = "childProductGroups", ignore = true)
    @Mapping(source = "parentProductGroupId", target = "parentProductGroup")
    ProductGroup toEntity(ProductGroupDTO productGroupDTO);

    default ProductGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProductGroup productGroup = new ProductGroup();
        productGroup.setId(id);
        return productGroup;
    }
}
