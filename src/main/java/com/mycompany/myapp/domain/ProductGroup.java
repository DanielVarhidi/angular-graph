package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ProductGroup.
 */
@Entity
@Table(name = "product_group")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProductGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_group_name")
    private String productGroupName;

    @Column(name = "jhi_type")
    private String type;

    @OneToMany(mappedBy = "productGroup")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Product> products = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("childProductGroups")
    private ProductGroup productGroup;

    @OneToMany(mappedBy = "productGroup")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ProductGroup> childProductGroups = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("")
    private ProductGroup parentProductGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductGroupName() {
        return productGroupName;
    }

    public ProductGroup productGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
        return this;
    }

    public void setProductGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
    }

    public String getType() {
        return type;
    }

    public ProductGroup type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public ProductGroup products(Set<Product> products) {
        this.products = products;
        return this;
    }

    public ProductGroup addProduct(Product product) {
        this.products.add(product);
        product.setProductGroup(this);
        return this;
    }

    public ProductGroup removeProduct(Product product) {
        this.products.remove(product);
        product.setProductGroup(null);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public ProductGroup getProductGroup() {
        return productGroup;
    }

    public ProductGroup productGroup(ProductGroup productGroup) {
        this.productGroup = productGroup;
        return this;
    }

    public void setProductGroup(ProductGroup productGroup) {
        this.productGroup = productGroup;
    }

    public Set<ProductGroup> getChildProductGroups() {
        return childProductGroups;
    }

    public ProductGroup childProductGroups(Set<ProductGroup> productGroups) {
        this.childProductGroups = productGroups;
        return this;
    }

    public ProductGroup addChildProductGroup(ProductGroup productGroup) {
        this.childProductGroups.add(productGroup);
        productGroup.setProductGroup(this);
        return this;
    }

    public ProductGroup removeChildProductGroup(ProductGroup productGroup) {
        this.childProductGroups.remove(productGroup);
        productGroup.setProductGroup(null);
        return this;
    }

    public void setChildProductGroups(Set<ProductGroup> productGroups) {
        this.childProductGroups = productGroups;
    }

    public ProductGroup getParentProductGroup() {
        return parentProductGroup;
    }

    public ProductGroup parentProductGroup(ProductGroup productGroup) {
        this.parentProductGroup = productGroup;
        return this;
    }

    public void setParentProductGroup(ProductGroup productGroup) {
        this.parentProductGroup = productGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProductGroup productGroup = (ProductGroup) o;
        if (productGroup.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), productGroup.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProductGroup{" +
            "id=" + getId() +
            ", productGroupName='" + getProductGroupName() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
