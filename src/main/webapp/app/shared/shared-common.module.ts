import { NgModule } from '@angular/core';

import { GraphSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [GraphSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [GraphSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class GraphSharedCommonModule {}
