import { IProduct } from 'app/shared/model//product.model';
import { IProductGroup } from 'app/shared/model//product-group.model';

export interface IProductGroup {
    id?: number;
    productGroupName?: string;
    type?: string;
    products?: IProduct[];
    productGroupId?: number;
    childProductGroups?: IProductGroup[];
    parentProductGroupId?: number;
}

export class ProductGroup implements IProductGroup {
    constructor(
        public id?: number,
        public productGroupName?: string,
        public type?: string,
        public products?: IProduct[],
        public productGroupId?: number,
        public childProductGroups?: IProductGroup[],
        public parentProductGroupId?: number
    ) {}
}
