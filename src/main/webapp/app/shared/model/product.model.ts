export interface IProduct {
    id?: number;
    productName?: string;
    type?: string;
    price?: number;
    productGroupId?: number;
    svgPosition?: number
}

export class Product implements IProduct {
    constructor(
        public id?: number,
        public productName?: string,
        public type?: string,
        public price?: number,
        public productGroupId?: number
    ) { }
}
