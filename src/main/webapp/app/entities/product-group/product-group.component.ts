import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { IProductGroup } from 'app/shared/model/product-group.model';
import { Principal } from 'app/core';
import { ProductGroupService } from './product-group.service';
import { D3Service } from '../d3/d3.service'

@Component({
    selector: 'jhi-product-group',
    templateUrl: './product-group.component.html'
})
export class ProductGroupComponent implements OnInit, OnDestroy {
    productGroups: IProductGroup[];
    currentAccount: any;
    eventSubscriber: Subscription;
    yearTimeout: any;


    constructor(
        private productGroupService: ProductGroupService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal,
        public d3Service: D3Service
    ) { }

    loadAll() {
        this.productGroupService.query().subscribe(
            (res: HttpResponse<IProductGroup[]>) => {
                this.productGroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInProductGroups();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.d3Service.productGroups = this.productGroups;
    }

    trackId(index: number, item: IProductGroup) {
        return item.id;
    }

    registerChangeInProductGroups() {
        this.eventSubscriber = this.eventManager.subscribe('productGroupListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
