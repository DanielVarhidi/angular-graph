import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProductGroup } from 'app/shared/model/product-group.model';
import { ProductGroupService } from './product-group.service';
import { ProductGroupComponent } from './product-group.component';
import { ProductGroupDetailComponent } from './product-group-detail.component';
import { ProductGroupUpdateComponent } from './product-group-update.component';
import { ProductGroupDeletePopupComponent } from './product-group-delete-dialog.component';
import { IProductGroup } from 'app/shared/model/product-group.model';
import { D3Component } from '../d3/app.component';

@Injectable({ providedIn: 'root' })
export class ProductGroupResolve implements Resolve<IProductGroup> {
    constructor(private service: ProductGroupService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((productGroup: HttpResponse<ProductGroup>) => productGroup.body));
        }
        return of(new ProductGroup());
    }
}

export const productGroupRoute: Routes = [
    {
        path: 'product-group',
        component: ProductGroupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProductGroups'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-group/:id/view',
        component: ProductGroupDetailComponent,
        resolve: {
            productGroup: ProductGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProductGroups'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-group/new',
        component: ProductGroupUpdateComponent,
        resolve: {
            productGroup: ProductGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProductGroups'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-group/:id/edit',
        component: ProductGroupUpdateComponent,
        resolve: {
            productGroup: ProductGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProductGroups'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-group/d3-view',
        component: D3Component,
        resolve: {
            productGroup: ProductGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProductGroups'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productGroupPopupRoute: Routes = [
    {
        path: 'product-group/:id/delete',
        component: ProductGroupDeletePopupComponent,
        resolve: {
            productGroup: ProductGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProductGroups'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
