import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GraphSharedModule } from 'app/shared';
import {
    ProductGroupComponent,
    ProductGroupDetailComponent,
    ProductGroupUpdateComponent,
    ProductGroupDeletePopupComponent,
    ProductGroupDeleteDialogComponent,
    productGroupRoute,
    productGroupPopupRoute
} from './';
import { AccordionModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/button';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { TableModule } from 'primeng/table';

const ENTITY_STATES = [...productGroupRoute, ...productGroupPopupRoute];

@NgModule({
    imports: [GraphSharedModule, RouterModule.forChild(ENTITY_STATES), AccordionModule, ButtonModule, ToggleButtonModule, TableModule],
    declarations: [
        ProductGroupComponent,
        ProductGroupDetailComponent,
        ProductGroupUpdateComponent,
        ProductGroupDeleteDialogComponent,
        ProductGroupDeletePopupComponent
    ],
    entryComponents: [
        ProductGroupComponent,
        ProductGroupUpdateComponent,
        ProductGroupDeleteDialogComponent,
        ProductGroupDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GraphProductGroupModule { }
