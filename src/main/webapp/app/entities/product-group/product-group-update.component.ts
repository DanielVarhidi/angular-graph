import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IProductGroup } from 'app/shared/model/product-group.model';
import { ProductGroupService } from './product-group.service';

@Component({
    selector: 'jhi-product-group-update',
    templateUrl: './product-group-update.component.html'
})
export class ProductGroupUpdateComponent implements OnInit {
    private _productGroup: IProductGroup;
    isSaving: boolean;

    productgroups: IProductGroup[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private productGroupService: ProductGroupService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ productGroup }) => {
            this.productGroup = productGroup;
        });
        this.productGroupService.query().subscribe(
            (res: HttpResponse<IProductGroup[]>) => {
                this.productgroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.productGroup.id !== undefined) {
            this.subscribeToSaveResponse(this.productGroupService.update(this.productGroup));
        } else {
            this.subscribeToSaveResponse(this.productGroupService.create(this.productGroup));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IProductGroup>>) {
        result.subscribe((res: HttpResponse<IProductGroup>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProductGroupById(index: number, item: IProductGroup) {
        return item.id;
    }
    get productGroup() {
        return this._productGroup;
    }

    set productGroup(productGroup: IProductGroup) {
        this._productGroup = productGroup;
    }
}
