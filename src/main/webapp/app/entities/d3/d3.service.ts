import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { IProductGroup } from 'app/shared/model/product-group.model';


@Injectable({ providedIn: 'root' })
export class D3Service {
    public productGroups: IProductGroup[];
}
