import { Node } from './';

export class Link implements d3.SimulationLinkDatum<Node> {
  // optional - defining optional implementation properties - required for relevant typing assistance
  index?: number;


  selected: boolean = false;
  hideMode: boolean = false;

  // must - defining enforced implementation properties
  source: Node | string | number;
  target: Node | string | number;

  constructor(source, target) {
    this.source = source;
    this.target = target;
  }

  get color() {
    let retVal = "rgb(0,130,60)"
    return retVal;
  }

  get opacity() {
    let opacityVal;
    if (this.hideMode && !this.selected) {
      opacityVal = "0.2";
    } else if (this.selected) {
      opacityVal = "1";
    } else {
      opacityVal = "1";
    }
    return opacityVal;
  }

  setSelected(selected: boolean) {
    this.selected = selected;
  }

  setHideMode(hideMode: boolean) {
    this.hideMode = hideMode;
  }

}
