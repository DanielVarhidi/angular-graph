import APP_CONFIG from '../../app.config';
import { IProduct } from 'app/shared/model//product.model';

export class Node implements d3.SimulationNodeDatum {
    // optional - defining optional implementation properties - required for relevant typing assistance
    index?: number;
    x?: number;
    y?: number;
    vx?: number;
    vy?: number;
    fx?: number | null;
    fy?: number | null;

    id: string;
    name?: string;
    type?: string;

    linkCount = 0;
    NODEHEIGHT = 40;
    NODEWIDTH = this.NODEHEIGHT * 3;
    private _nodeHeight: number = this.NODEHEIGHT;
    private _nodeWidth: number = this.NODEWIDTH;
    TOOLTIPHEIGHT = 20;
    TOOLTIPWIDTH: number = this.TOOLTIPHEIGHT * 3;
    private _toolTipHeight: number = this.TOOLTIPHEIGHT;
    private _toolTipWidth: number = this.TOOLTIPWIDTH;
    isInConnection = true;

    linkTo: Node[] = [];
    selected = false;
    hideMode = false;

    details: IProduct[];
    toolTip: any[] = [];

    constructor(id, name, type, details) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.details = details;
    }

    normal = () => {
        return Math.sqrt(this.linkCount / APP_CONFIG.N);
    };

    get r() {
        return 60;
    }

    get fontSize() {
        return 16;
    }

    get color() {
        const colVal = 'rgb(49,162,20)';
        return colVal;
    }

    get opacity() {
        let opacityVal;
        if (this.hideMode && !this.selected) {
            opacityVal = '0.5';
        } else if (this.selected) {
            opacityVal = '1';
        } else {
            opacityVal = '1';
        }
        return opacityVal;
    }

    get nodeHeight() {
        if (this._nodeHeight !== this.NODEHEIGHT) {
            return this._nodeHeight;
        }
        return this.NODEHEIGHT;
    }

    get nodeWidth() {
        if (this._nodeWidth !== this.NODEWIDTH) {
            return this._nodeWidth;
        }
        return this.NODEWIDTH;
    }

    get toolTipHeight() {
        if (this._toolTipHeight !== this.TOOLTIPHEIGHT) {
            return this._toolTipHeight;
        }
        return this.TOOLTIPHEIGHT;
    }

    get toolTipWidth() {
        if (this._toolTipWidth !== this.TOOLTIPWIDTH) {
            return this._toolTipWidth;
        }
        return this.TOOLTIPWIDTH;
    }

    setIsInConnection(inConnection: boolean) {
        this.isInConnection = inConnection;
    }

    setNodeHeight(height: number) {
        this._nodeHeight = height;
    }

    setNodeWidth(width: number) {
        this._nodeWidth = width;
    }

    setToolTipHeight(height: number) {
        this._toolTipHeight = height;
    }

    setToolTipWidth(width: number) {
        this._toolTipWidth = width;
    }

    setSelected(selected: boolean) {
        this.selected = selected;
    }

    setLinkToSelected(selected: boolean) {
        this.selected = selected;
        for (const n of this.linkTo) {
            n.setSelected(selected);
        }
    }

    setHideMode(hideMode: boolean) {
        this.hideMode = hideMode;
    }
}
