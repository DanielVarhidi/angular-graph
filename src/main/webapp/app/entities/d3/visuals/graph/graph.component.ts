import { Component, Input, ChangeDetectorRef, HostListener, ChangeDetectionStrategy, OnInit, AfterViewInit } from '@angular/core';
import { D3Service, ForceDirectedGraph, Node } from '../../d3';

@Component({
  selector: 'graph',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <svg #svg [attr.width]="_options.width" [attr.height]="_options.height">
      <g [zoomableOf]="svg">
        <g [linkVisual]="link" *ngFor="let link of links"></g>
        <g [graph]="this" [nodeVisual]="node" *ngFor="let node of nodes"
            [draggableNode]="node" [draggableInGraph]="graph"></g>
      </g>
    </svg>
  `,
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit, AfterViewInit {
  @Input('nodes') nodes;
  @Input('links') links;
  @Input('toolTipArray') toolTipArray;
  graph: ForceDirectedGraph;
  private _options: { width, height } = { width: 800, height: 600 };

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.graph.initSimulation(this.options);
  }

  constructor(private d3Service: D3Service, private ref: ChangeDetectorRef) { }

  findLinkLinkTo(node: Node, b: boolean) {
    for (let i = 0; i < this.links.length; i++) {
      const link = this.links[i];
      if (link.source.id === node.id) {
        link.setHideMode(b);
        link.setSelected(b);
      } else {
        let o = !b;
        link.setHideMode(b);
        link.setSelected(o);
      }
    }
  }

  setBasicLinkColor() {
    for (let i = 0; i < this.links.length; i++) {
      const link = this.links[i];
      link.setHideMode(false);
      link.setSelected(false);
    }
  }

  ngOnInit() {
    this.findNodeLinkTo();
    this.findToolTip();

    /** Receiving an initialized simulated graph from our custom d3 service */
    this.graph = this.d3Service.getForceDirectedGraph(this.nodes, this.links, this.options);

    /** Binding change detection check on each tick
     * This along with an onPush change detection strategy should enforce checking only when relevant!
     * This improves scripting computation duration in a couple of tests I've made, consistently.
     * Also, it makes sense to avoid unnecessary checks when we are dealing only with simulations data binding.
     */
    this.graph.ticker.subscribe((d) => {
      this.ref.markForCheck();
    });
  }

  ngAfterViewInit() {
    this.graph.initSimulation(this.options);
  }

  get options() {
    return this._options = {
      width: window.innerWidth,
      height: window.innerHeight
    };
  }


  hideMode(hideMode: boolean) {
    for (let n of this.nodes) {
      n.setHideMode(hideMode);
    }
  }

  findNodeLinkTo() {
    for (let j = 0; j < this.links.length; j++) {
      const node = this.nodes.find(fn => fn.id === this.links[j].source);
      node.linkTo.push(this.nodes.find(fn => fn.id === this.links[j].target));
    }
  }

  findToolTip() {
    for (let i = 0; i < this.toolTipArray.length; i++) {
      const toolTip = this.toolTipArray[i];
      for (let j = 0; j < this.nodes.length; j++) {
        const node = this.nodes[j];
        if (toolTip.id == node.id) {
          node.toolTip.push(toolTip);
        }
      }
    }
  }
}
