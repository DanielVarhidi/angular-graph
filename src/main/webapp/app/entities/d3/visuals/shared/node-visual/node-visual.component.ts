import { Component, Input } from '@angular/core';
import { Node } from '../../../d3';
import { Link } from '../../../d3';
import { ForceDirectedGraph } from '../../../d3/models/force-directed-graph';
import { DataService } from './data.service';
import { GraphComponent } from '../../graph/graph.component';

@Component({
    selector: '[nodeVisual]',
    templateUrl: './node-visual.component.html',
    styleUrls: ['./node-visual.component.css']
})
export class NodeVisualComponent {
    @Input('graph') graph: GraphComponent;
    @Input('nodeVisual') node: Node;
    ATTRY = 25;
    isShowDetails = false;
    isHasDetails = false;
    isShowToolTip = false;
    isHasToolTip = false;

    ngOnInit() {
        this.setRectWidth();
        this.increaseRectangleWidth();
        this.changeToolTipRectangleSize();
    }

    setRectWidth() {
        const stringLength: number = this.node.name.length * 14;
        this.node.setNodeWidth(stringLength);
    }

    increaseRectangleWidth() {
        if (this.node.details.length > 0) {
            for (const element of this.node.details) {
                this.isHasDetails = true;
                const stringLength: number = element.productName.length * 14;
                if (stringLength > this.node.nodeWidth) {
                    this.node.setNodeWidth(stringLength);
                }
            }
        }
    }

    /* Details */

    showDetails(nodeId) {
        if (this.node.id === nodeId && this.isShowDetails === false) {
            this.isShowDetails = true;
            this.increaseNodeHeight();
        } else {
            this.isShowDetails = false;
            this.decreaseNodeHeight();
        }
    }

    increaseNodeHeight() {
        for (let i = 0; i < this.node.details.length; i++) {
            const element = this.node.details[i];
            element.svgPosition = i + 1;
        }
        const height = this.node.details.length;
        this.node.setNodeHeight(this.node.NODEHEIGHT + height * this.ATTRY);
    }
    decreaseNodeHeight() {
        this.node.setNodeHeight(this.node.NODEHEIGHT);
    }

    /* ToolTip */

    changeToolTipRectangleSize() {
        for (let i = 0; i < this.node.toolTip.length; i++) {
            const element = this.node.toolTip[i];
            if (element.id == this.node.id) {
                this.hasToolTip(element);
                this.setToolTipWidth(element);
                this.setToolTipHeight(element);
            }
        }
    }

    hasToolTip(element) {
        this.isHasToolTip = true;
    }

    setToolTipWidth(element) {
        let stringLengthMax = 0;
        for (let j = 0; j < element.dataList.length; j++) {
            const subElement = element.dataList[j];
            // TODO: String szélességét megkapni pixelben
            const stringLength = subElement.data.length * 6;
            if (stringLength > stringLengthMax) {
                stringLengthMax = stringLength;
            }
        }
        if (stringLengthMax > this.node.TOOLTIPWIDTH) {
            this.node.setToolTipWidth(stringLengthMax + 5);
        }
    }

    setToolTipHeight(element) {
        const height = element.dataList.length * 12;
        if (height > this.node.TOOLTIPHEIGHT) {
            this.node.setToolTipHeight(height * 3);
        }
    }

    /* Show Connections */

    hideNodeLinkTo(nodeId: number) {
        const f = false;
        this.isShowToolTip = f;
        this.node.setLinkToSelected(f);
        this.graph.hideMode(f);
        this.graph.setBasicLinkColor();
    }

    showNodeLinkTo(nodeId: number) {
        const t = true;
        this.isShowToolTip = t;
        this.node.setLinkToSelected(t);
        this.graph.hideMode(t);
        this.graph.findLinkLinkTo(this.node, t);
    }
}
