import { Component, Input } from '@angular/core';
import { Link } from '../../../d3';
import { GraphComponent } from '../../graph/graph.component';

@Component({
  selector: '[linkVisual]',
  template: `

    <svg:defs>
      <svg:marker id="markerArrow" markerWidth="13" markerHeight="13" refX="2" refY="6" orient="auto">
          <svg:path d="M2,2 L2,11 L10,6 L2,2" [attr.fill]="link.color" />
      </svg:marker>
    </svg:defs>

    <svg:line
        class="link"
        [attr.stroke]="link.color"
        [attr.opacity]="link.opacity"
        [attr.x1]="link.source.x"
        [attr.y1]="link.source.y"
        [attr.x2]="link.target.x"
        [attr.y2]="link.target.y"
    ></svg:line>

  `,
  styleUrls: ['./link-visual.component.css']
})
export class LinkVisualComponent {
  @Input('linkVisual') link: Link;

}
