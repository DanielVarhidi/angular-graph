import { Component } from '@angular/core';
import APP_CONFIG from './app.config';
import { Node, Link } from './d3';
import { IProductGroup } from 'app/shared/model/product-group.model';
import { group } from '@angular/animations';
import { D3Service } from './d3.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class D3Component {
  nodes: Node[] = [];
  links: Link[] = [];
  toolTipArray: any[] = [];
  productGroups: IProductGroup[] = [];
  testString: string;
  N = APP_CONFIG.N;

  ngOnInit() {
    this.productGroups = this.d3Service.productGroups;
    this.loadNodeToolTip();
    this.loadNode();
    this.loadLink();
  }

  constructor(
    public d3Service: D3Service
  ) { }

  getIndex = number => number - 1;

  loadNode() {
    for (let node of this.productGroups) {
      this.nodes.push(new Node(node.id, node.productGroupName, node.type, node.products))
    }
  }

  loadLink() {
    for (let node of this.productGroups) {
      if (node.productGroupId !== null) {
        this.links.push(new Link(node.productGroupId, node.id));
      }
    }
  }

  loadNodeToolTip() {
    this.toolTipArray = [
      {
        id: "1",
        dataList: [
          { id: 1, data: "Lorem ipsum dolor sit amet, consectetur adipiscing elit." }
        ]
      },
      {
        id: "4",
        dataList: [
          { id: 1, data: "Nam tempor sed quam ac vulputate." },
          { id: 2, data: "Donec consequat eleifend mauris" }
        ]
      },
      {
        id: "3",
        dataList: [
          { id: 1, data: "Sed scelerisque at elit ac viverra. Quisque tellus velit, tempor non sapien quis, pretium tristique libero. Nullam at auctor metus, id vehicula arcu." }
        ]
      }
    ];
  }
}
