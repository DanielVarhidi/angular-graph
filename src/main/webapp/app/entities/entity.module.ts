import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { GraphProductModule } from './product/product.module';
import { GraphProductGroupModule } from './product-group/product-group.module';
import { D3Module } from './d3/app.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        GraphProductModule,
        GraphProductGroupModule,
        D3Module
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GraphEntityModule { }
